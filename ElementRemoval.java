import java.util.Arrays;

public class ElementRemoval {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3};
        int element = 1;
        int[] results = new int[nums.length];
        boolean isFound = false;
        // Algorithm to remove the nums from an array
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == element) {
                isFound = true;
            }
            if (isFound && i <= nums.length - 1) {
                results[i] = nums[i + 1];
            }
            else {
                results[i] = nums[i];
            }
        }
        // Print out the results
        System.out.println(Arrays.toString(results));
    }
}
